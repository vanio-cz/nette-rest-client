<?php

namespace Vanilla\NetteRestClient;

use Nette\Diagnostics\Debugger;

/**
 * Result
 * 
 * @author     Pavel Lauko
 * @package    Fakturant
 */
class Result extends \Nette\Object {

	/** @var StdClass */
	public $data;
	/** @var string */
	private $accessToken;
	
	/**
	 * Contructor
	 * 
	 * @param StdClass $result
	 */
	public function __construct($result) {
		$this->data = $result;
	}
	
	/**
	 * Get data from response
	 * 
	 * @return \RestApi\Data|null
	 */
	public function data() {
		if(!Empty($this->data->data)) {
		
			$clone = new Data();
		    foreach ($this->data->data as $key => $val) {
		        $clone->{$key} = $val;
		    }
			return $clone;
		}
		return NULL;
	}
	
	/**
	 * Get data from response
	 * 
	 * @return \RestApi\Data|null
	 */
	public function rawData() {
		return $this->data;
	}
	
	/**
	 * Get Access token from response
	 * @return null
	 */
	public function accessToken() {
		if(!Empty($this->data->accessToken)) {
			return $this->data->accessToken;
		}
		return NULL;
	}	
	
	/**
	 * 
	 * @return null|StdClass
	 */
	public function getResult() {
		if(!Empty($this->data->result)) {
			return $this->data->result;
		}
		return NULL;
	}
}