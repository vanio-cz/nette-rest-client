<?php

namespace Vanilla\NetteRestClient;

use Nette\Diagnostics\Debugger;
use Nette\DI\Container;

/**
 * REST API
 * 
 * @author     Pavel Lauko
 * @package    Fakturant
 */
class RestApi extends \Nette\Object {

	/** @var string */
	public $api_key;
	
	/** @var string */	
	private $format = "json";
	
	/** @var \RestApi\Result */
	protected $result;
	
	/** @var string */
	private $api_url;
	
	/** @var string */
	private $request_url;
	
	/** @var string */
	private $api_url_final;
	
	/** @var \Nette\DI\Container */
	private $context;
	
	/** @var string */
	private $access_token;
	
	/** @var string */
	private $post_params;
	
	/** @var string */
	private $get_params;
	
	/** @var string */
	private $http_code;

	/**
	 * Constructor
	 * 
	 * @param \Nette\DI\Container $container
	 * @param array $config
	 */
	public function __construct(Container $container, array $config) {
		$this->context = $container;
		$this->setApiUrl($config["apiFaUri"]);
		$this->login($config["userName"],$config["apiKey"]);
	}
	
	/**
	 * Set API Url
	 * @param string $uri
	 */
	public function getApiUrl() {
		return $this->api_url_final;
	}
	
	/**
	 * Set API Url
	 * @param string $uri
	 */
	public function setApiUrl($uri) {
		$this->api_url = $uri;
		return $this;
	}
	
	/**
	 * Set format
	 * @param string $format
	 */
	public function setFormat($format) {
		$this->format = $format;
		return $this;
	}
	
	/**
	 * Get access token
	 * 
	 * @param string $userName
	 * @param string $apiKey
	 * @return boolean|string
	 */
	public function login($userName,$apiKey) {
		$params = array (
			'userName' => $userName,
			'apiKey' => $apiKey
		);
		$token = $this->get('auth/get-access-token',$params);
		$accessToken = $token->accessToken();
		if(!Empty($accessToken)) {
			$this->access_token = $accessToken;
			return $this->access_token;
		} else {
			$this->access_token = null;
			return false;
		}
	}	
	
	/**
	 * Make HTTP GET request 
	 * 
	 * @param string $resource
	 * @param array $params
	 * @return \RestApi\Result
	 */
	public function get($resource, $params = array()) {
	
		$url = $this->buildApiUrl($resource);
		
		if(!is_null($this->access_token)) {
			$params['accessToken'] = $this->access_token;
		}
		
		$this->result  = new Result($this->request($url, array('get' => $params)));

		return $this->result;
	}
	
	/**
	 * Make HTTP POST request 
	 * 
	 * @param string $resource
	 * @param array $params
	 * @return \RestApi\Result
	 */
	public function post($resource, $params = array()) {
		$url = $this->buildApiUrl($resource);
		
		$get = array();
		if(!is_null($this->access_token)) {
			$get['accessToken'] = $this->access_token;
		}
		
		$this->result  = new Result($this->request($url, array('post' => $params, 'get' => $get)));
		return $this->result;
	}
	
	/**
	 * 
	 * @param type $resource
	 * @return type
	 */
	public function buildApiUrl($resource) {
		return $this->api_url.$resource;
	}
	
	/**
	 * 
	 * @param string $url
	 * @param array $extra
	 * @return boolean|mixed
	 */
	protected function request($url, $extra = array()) {
		if (isset($extra['get']) && is_array($extra['get']) && (count($extra['get']) > 0)) {
			$url .= '?';
			$url .= http_build_query($extra['get']);
			$this->get_params = $extra['get'];
		}
			
		if (isset($extra['post'])) {
			if (is_array($extra['post']) && count($extra['post'] > 0))
			{
				$post = "";
				$first = true;
				foreach ($extra['post'] as $param=>$value)
				{
					if (!$first)
						$post .= '&';
					else
						$first = false;
					$post .= urlencode($param) . '=' . urlencode($value);
				}
			} elseif (is_string($extra['post'])) {
				$post = $extra['post'];
			}
			$this->post_params = $extra['post'];
		}
		else {
			$post = false;
		}

		$this->request_url = Trim($url,"?");
		$ch = \curl_init(Trim($this->request_url,"?"));
		$this->api_url_final = $this->request_url;
		if($post !== false) {
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);			
        }
		
		if (isset($extra['headers']) && is_array($extra['headers']) && count($extra['headers'] > 0))
		{
			curl_setopt($ch, CURLOPT_HTTPHEADER, $extra['headers']);
		}
		
		$this->setCurlOpts($ch);
		
        $response = curl_exec($ch);
       	$info = curl_getinfo($ch);
       	curl_close($ch);
		
       	$this->http_code = $info['http_code'];
		
       	$object = $this->verify($info, $response);
			
		if ($object !== false && !is_null($object))
		{
			return $object;
		}
		
		return false;
	}
	
	/**
	 * Verify response 
	 * 
	 * @param type $info
	 * @param type $response
	 * @return boolean|mixed
	 */
	private function verify($info, $response) {
		if (!preg_match('/^2[0-9]{2}$/', $info['http_code']))
			return false;
		
		if ($response === false)
			return false;

		return $this->objectify($response);
	}
	
	/**
	 * Set CURL params
	 * 
	 * @param curl $ch
	 */
	private function setCurlOpts($ch)
	{
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 100);
		curl_setopt($ch, CURLOPT_TIMEOUT, 100);
	}
	
	/**
	 * Objectify request response
	 * 
	 * @param string $response
	 * @return mixed
	 */
	private function objectify($response)
	{
		switch ($this->format)
		{
			case 'json':
			case 'js':
				return json_decode($response);
				break;
			case 'xml':
			case 'atom':
			case 'rss':
				return simplexml_load_string($response);
				break;
			case 'php':
			case 'php_serial':
				return unserialize($response);
			default:
				return $response;
		}
	}
	
	/**
	 * Returns info about request and response
	 * 
	 * @return \RestApi\Mirror
	 */
	public function info () {
		$mirror = new Mirror();
		$mirror->url = $this->request_url;
		$mirror->get = $this->get_params;
		$mirror->post = $this->post_params;
		$mirror->response = $this->result;	
		$mirror->http_code = $this->http_code;
		return $mirror;
	}
}