<?php

namespace Vanilla\NetteRestClient;

use Nette\Diagnostics\Debugger;

/**
 * RestMirror
 * 
 * @author     Pavel Lauko
 * @package    Fakturant
 */
class Mirror extends \Nette\Object {
	
	public $url;
	public $get;
	public $post;
	public $response;
	public $http_code;
	
}