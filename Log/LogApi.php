<?php

namespace Vanilla\NetteRestClient\Log;

use Nette\Diagnostics\Debugger;

/**
 * Log Cron API Model
 *
 * @author		Pavel Lauko
 * @package    Fakturant
 */
class LogApiModel extends LogModel {
	
	/**
	 * Log action to database
	 * 
	 * @param \RestApi\Mirror
	 * @param string
	 * @return bool
	 */
	public function save($apiMirror, $type) {	
		
		$this->setType($type)
			->setGetParams($apiMirror->get)
			->setPostParams($apiMirror->post)
			->setResponse(json_encode($apiMirror->response->data()))
			->setUrl($apiMirror->url)
			->log();
	} 
	
	/**
	 * Save all added logs to database
	 */
	public function saveAll() {		
		return $this->logAll();
	} 
	
	/**
	 * Add new log to array 
	 * 
	 * @param \RestApi\Mirror
	 * @param string
	 * @return int
	 */
	public function add($apiMirror, $type) {	
		if($apiMirror instanceof \Vanilla\NetteRestClient\Mirror) {		
			$this->data[] = array(
				'type' => $type,
				'get' => json_encode($apiMirror->get),
				'post' => json_encode($apiMirror->post),
				'response' => json_encode($apiMirror->response->data()),
				'url' => $apiMirror->url,			
			);		
			return Count($this->data);
		} else {
			return 0;
		}
	}
}