<?php

namespace Vanilla\NetteRestClient\Log;

/**
 * Logging received API requests
 *
 * @author		Pavel Lauko
 * @package    Fakturant
 */
class LogApiReceivedModel extends LogApiModel {
	
	/** @var string */
	protected static $table = 'log_received';
	
	/**
	 * Log action to database
	 * 
	 * @param array
	 * @param string
	 * @return bool
	 */
	public function save( $apiMirror, $type) {		
		$this->setType($type)
			->setGetParams($apiMirror['get'])
			->setPostParams($apiMirror['post'])
			->setResponse(json_encode($apiMirror['data']))
			->setUrl($apiMirror['url'])
			->log();
	} 
	
	/**
	 * Add new log to array 
	 * 
	 * @param array
	 * @param string
	 * @return int
	 */
	public function add( $apiMirror, $type) {		
		$this->data[] = array(
			'type' => $type,
			'get' => json_encode($apiMirror['get']),
			'post' => json_encode($apiMirror['post']),
			'response' => json_encode($apiMirror['data']),
			'url' => $apiMirror['url'],			
		);		
		return Count($this->data);
	}
}