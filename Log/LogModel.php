<?php

namespace Vanilla\NetteRestClient\Log;

use Nette\Diagnostics\Debugger;
use \Nette\DI\Container;
/**
 * Log Model
 *
 * @author		Pavel Lauko
 * @package    Fakturant
 */
class LogModel {
	
	/** @var string */
	protected static $table = 'log';
	
	/** @var Nette\Database\Connection */
	protected $db;
	
	protected $data = array();
		
	/** @var Nette\DI\Container */
	protected $context;
	
	/** @var string */
	protected $type = NULL;
	
	/** @var string */
	protected $url;
	
	/** @var string */
	protected $get_params;
	
	/** @var string */
	protected $post_params;
	
	/** @var string */
	protected $response;
	
	/**
	 * Constructor
	 * 
	 * @param \Nette\DI\Container $context
	 */
	public function __construct(Container $context) {
		$this->context = $context;
		$this->db = $context->getService('database');
	}	
	
	/**
	 * Set log type
	 * @param string $type
	 * @return LogModel
	 */
	public function setType($type) {
		$this->type = $type;
		return $this;
	}
	
	/**
	 * Set request URI
	 * @param string $url
	 * @return LogModel
	 */
	public function setUrl($url) {
		$this->url = $url;
		return $this;
	}
	
	/**
	 * Set GET parameters
	 * @param string $get_params
	 * @return LogModel
	 */
	public function setGetParams($get_params) {
		$this->get_params = $get_params;
		return $this;
	}
	
	/**
	 * Set POST parameters
	 * @param string $post_params
	 * @return LogModel
	 */
	public function setPostParams($post_params) {
		$this->post_params = $post_params;
		return $this;
	}
	
	/**
	 * Set requst response
	 * @param string $response
	 * @return LogModel
	 */
	public function setResponse($response) {
		$this->response = $response;
		return $this;
	}
	
	/**
	 * Log action to database
	 * 
	 * @return bool
	 * @throws ApiLogException
	 */
	public function log() {
		
		if(!$this->type) {
			throw new ApiLogException('Log type must be specified.');
		}
		$values = array(
			'type' =>$this->type,
			'request_url' =>$this->url,
			'request_get_params' =>json_encode($this->get_params),
			'request_post_params' =>json_encode($this->post_params),
			'request_response' =>$this->response,
			'date_added' => new \DateTime(),
		);		
		$result = $this->db->table(self::$table)->insert($values);
		return $result;
	} 
	
	/**
	 * Log all action to database
	 * 
	 * @return bool
	 * @throws ApiLogException
	 */
	public function logAll() {
		$values = array();
		foreach ($this->data as $id => $row) {
			if(!$row['type']) {
				throw new ApiLogException('Log type must be specified.');
			}
			$values[] = array(
				'type' =>$row['type'],
				'request_url' =>$row['url'],
				'request_get_params' =>$row['get'],
				'request_post_params' =>$row['post'],
				'request_response' =>$row['response'],
				'date_added' => new \DateTime(),
			);			
		}
		$result = $this->db->table(self::$table)->insert($values);
		return $result;
	} 
}

class ApiLogException extends \Exception {
	
}


